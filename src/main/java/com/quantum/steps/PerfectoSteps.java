package com.quantum.steps;

import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.quantum.utils.DeviceUtils;
import com.quantum.utils.ReportUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;

import java.util.HashMap;
import java.util.Map;

import static com.qmetry.qaf.automation.core.ConfigurationManager.getBundle;

@QAFTestStepProvider
public class PerfectoSteps
{
  @Then("^I want to see the image \"(.*)\" with a threshold of \"(.*)\"$")
  public void verifyImageWithThreshold(String img, String threshold)
  {
    String context = DeviceUtils.getCurrentContext();
    DeviceUtils.switchToContext("VISUAL");
    Map<String, Object> params = new HashMap();
    params.put("content", img);
    params.put("measurement", "accurate");
    params.put("source", "primary");
    params.put("threshold", threshold);
    params.put("timeout", 15);
    params.put("match", "bounded");
    //params.put("imageBounds.needleBound", 25);
    Object result = DeviceUtils.getQAFDriver().executeScript("mobile:checkpoint:image", new Object[]{params});
    DeviceUtils.switchToContext(context);

    Validator.verifyThat("Image " + img + " should be visible", result.toString(), Matchers.equalTo("true"));
  }

  @Given("^I try to close application by name \"(.*)\" and ignore error")
  public void ITryToCloseApplicationByNameAndIgnoreError(String name)
  {
    DeviceUtils.closeApp(name, "name", true);
  }

  @Given("^I try to close application by id \"(.*)\" and ignore error")
  public void ITryToCloseApplicationByIDAndIgnoreError(String id)
  {
    DeviceUtils.closeApp(id, "identifier", true);
  }

  @Given("^I am on \"(.*?)\"$")
  public void IAmOn(String url) throws Throwable
  {
    new WebDriverTestBase().getDriver().get(url);
  }

  @When("^I click on \"(.*)\" when visible$")
  public void IClickOnWhenVisible(String loc)
  {
    QAFExtendedWebElement element = new QAFExtendedWebElement(loc);
    element.waitForVisible(10000);
    element.click();
  }

  @When("^I click \"(.*)\" using JavaScript")
  public void IClickUsingJavaScript(String loc)
  {
    QAFExtendedWebElement element = new QAFExtendedWebElement(loc);
    JavascriptExecutor js = DeviceUtils.getQAFDriver();
    js.executeScript("arguments[0].click();", element);
  }

  @When("I press on back button")
  public void IPressOnBackButton()
  {
    DeviceUtils.getQAFDriver().navigate().back();
  }

  @When("I maximize window")
  public void IMaximizeWindow()
  {
    DeviceUtils.getQAFDriver().manage().window().maximize();
  }

  @Given("^I clean Android app by name \"(.*)\"")
  public void ICleanAndroidAppByName(String name)
  {
    if (DeviceUtils.getDeviceProperty("os").contains("Android"))
    {
      DeviceUtils.cleanApp(name, "name");
    }
    else
    {
      ReportUtils.logVerify("Only supported on Android. Nothing done.", true);
    }
  }

    @Then("^I zoom in")
    public void IZoomIn()
    {
        Map<String, Object> params1 = new HashMap<>();
        params1.put("start", "40%,40%");
        params1.put("end", "20,20%");
        params1.put("operation", "zoom");
        Object result1 = DeviceUtils.getQAFDriver().executeScript("mobile:touch:gesture", params1);
    }

    @Then("^I zoom out")
    public void IZoomOut()
    {
        Map<String, Object> params1 = new HashMap<>();
        params1.put("start", "20%,20%");
        params1.put("end", "20,40%");
        params1.put("operation", "zoom");
        Object result1 = DeviceUtils.getQAFDriver().executeScript("mobile:touch:gesture", params1);
    }

    @Then("^I get my phone number$")
    public void IgetMyPhoneNo() throws Exception {
        DeviceUtils.switchToContext("NATIVE_APP");
        String phoneNumber = DeviceUtils.getDeviceProperty("phoneNumber").replaceAll("/(?<!^)\\+|[^\\d+]+/","");
        getBundle().setProperty("phoneNumber",phoneNumber);
    }
}
