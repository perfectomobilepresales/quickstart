package com.quantum.steps;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.quantum.utils.DeviceUtils;

import com.quantum.utils.DriverUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.bytebuddy.matcher.CollectionOneToOneMatcher;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;

@QAFTestStepProvider
public class CommonSteps {

    @Then("^I click on \"(.*?)\" and ignore errors$")
    public static void iClickAndIgnore(String locator) {
        try {
            CommonStep.click(locator);
        } catch (Exception ex)
        {;}
    }

    @Then("^I scroll up a bit$")
    public static void IScrollUpABit()
    {
        DeviceUtils.swipe("50%,40%", "50%,60%");
    }

    @Then("^I scroll down a bit$")
    public static void IScrollDownABit()
    {

        if (DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.IOS) ||
                DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.ANDROID)) {
            DeviceUtils.swipe("50%,60%", "50%,40%");
        }
    }

    @Then("^I scroll up a lot$")
    public static void IScrollUpALot()
    {
        DeviceUtils.swipe("50%,20%", "50%,80%");
    }

    @Then("^I scroll down a lot$")
    public static void IScrollDownALot()
    {
        DeviceUtils.swipe("50%,90%", "50%,10%");
    }

    @Then("^I swipe up a lot$")
    public static void ISwipeUpALot()
    {
            DeviceUtils.swipe("50%,50%", "50%,10%");
    }

    @Then("^I swipe up a bit$")
    public static void ISwipeUpABit()
    {
        DeviceUtils.swipe("50%,50%", "50%,30%");
    }

    @Then("^I swipe down a lot$")
    public static void ISwipeDownALot()
    {
        DeviceUtils.swipe("50%,50%", "50%,90%");
    }

    @Then("^I swipe left a lot$")
    public static void ISwipeLeftALot()
    {
        DeviceUtils.swipe("90%,40%", "10%,40%");
    }

    @Then("^I swipe right a lot$")
    public static void ISwipeRightALot()
    {
        DeviceUtils.swipe("10%,60%", "90%,60%");
    }

    @Then("^I swipe to go back$")
    public static void ISwipe2GoBack()
    {
        DeviceUtils.swipe("1%,50%", "60%,50%");
    }

    @Then("^I open control centre$")
    public static void IOpenControlCentre()
    {
        if (DeviceUtils.getDeviceProperty("os").contains("Android")) {
            ;
        } else if (DeviceUtils.getDeviceProperty("os").contains("iOS")) {
            DeviceUtils.swipe("90%,1%", "90%,50%");
        }
    }

    @Then("^I answer phone call$")
    public static void IAnswerPhoneCall()
    {
        try {
            QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
            if (DeviceUtils.getDeviceProperty("os").contains(Constants.ANDROID)) {
                Map<String, Object> params7 = new HashMap<>();
                params7.put("label", "ANSWER");
                params7.put("interval", "1");
                params7.put("timeout", "10");
                params7.put("ignorecase", "nocase");
                Object result7 = driver.executeScript(Constants.MOBILEBUTTONTEXTCLICK, params7);
            } else if (DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.IOS)) {
                Map<String, Object> params9 = new HashMap<>();
                params9.put("label", "PRIVATE:script/AnswerButton.png");
                params9.put("timeout", "10");
                Object result9 = driver.executeScript(Constants.MOBILEBUTTONIMAGECLICK, params9);
            }
        } catch (Exception e) {;}
    }

    @Then("^I end phone call$")
    public static void IEndPhoneCall()
    {
        try {
            QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
            if (DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.ANDROID)) {
                DeviceUtils.switchToContext(Constants.NATIVEAPP);
                driver.findElementByXPath("//*[@resource-id=\"com.samsung.android.incallui:id/endCallButton\"]").click();
            } else if (DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.IOS)) {
                Map<String, Object> params10 = new HashMap<>();
                params10.put("label", "PRIVATE:script/EndCallButton.png");
                Object result10 = driver.executeScript(Constants.MOBILEBUTTONIMAGECLICK, params10);
            }
        } catch (Exception e) {;}
    }

    @Then("^I turn on WiFi$")
    public static void ITurnOnWiFi()
    {
        try {
            QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
            if (DeviceUtils.getDeviceProperty("os").contains("Android")) {
                Map<String,Object> params1 = new HashMap<>();
                params1.put("wifi", "enabled");
                Object res1 = driver.executeScript(Constants.MOBILENETWORKSETTINGSSET, params1);
                Object res2 = driver.executeScript(Constants.MOBILENETWORKSETTINGSSET, params1);
            } else if (DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.IOS)) {
                IOpenControlCentre();
                Map<String, Object> params9 = new HashMap<>();
                params9.put("label", "PRIVATE:script/WiFiOff.png");
                params9.put("timeout", "5");
                Object result9 = driver.executeScript(Constants.MOBILEBUTTONIMAGECLICK, params9);
                IScrollUpABit();
            }
        } catch (Exception e) {;}
    }

    @Then("^I turn off WiFi$")
    public static void ITurnOffWiFi()
    {
        try {
            QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
            if (DeviceUtils.getDeviceProperty("os").contains("Android")) {
                Map<String,Object> params1 = new HashMap<>();
                params1.put("wifi", "disabled");
                Object res1 = driver.executeScript(Constants.MOBILENETWORKSETTINGSSET, params1);
                Object res2 = driver.executeScript(Constants.MOBILENETWORKSETTINGSSET, params1);
            } else if (DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.IOS)) {
                IOpenControlCentre();
                Map<String, Object> params9 = new HashMap<>();
                params9.put("label", "PRIVATE:script/WiFi.png");
                params9.put("timeout", "5");
                Object result9 = driver.executeScript(Constants.MOBILEBUTTONIMAGECLICK, params9);
                IScrollUpABit();
            }
        } catch (Exception e) {;}
    }

    @Then("^I install app \"(.*?)\"$")
    public static void IInstallApp(String filePath) {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        Map<String, String> params = new HashMap<>();
        params.put("file", filePath);
        params.put("instrument", "instrument");
        params.put("sensorInstrument", "sensor");
        params.put("resign","true");
        driver.executeScript(Constants.MOBILEAPPLICATIONINSTALL, new Object[]{params});
    }

    @Then("^I uninstall app \"(.*?)\"$")
    public static void uninstallAppIgnoreErrors(String name) {
        try
        {
            DeviceUtils.uninstallApp(name, "name");
        } catch (Exception ex)
        {;}
    }

    @Then("^I zoom in on map")
    public static void IZoomInOnMap()
    {
        Map<String, Object> params1 = new HashMap<>();
        if (DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.IOS)) {
            params1.put("start", "50%,55%");
            params1.put("end", "50%,75%");
        } else if (DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.ANDROID)) {
            params1.put("start", "50%,45%");
            params1.put("end", "50%,65%");
        }
        params1.put("operation", "zoom");
        Object result1 = DeviceUtils.getQAFDriver().executeScript(Constants.MOBILETOUCHGESTURE, params1);
    }

    @Then("^I zoom out on map")
    public static void IZoomOutnOnMap()
    {
        Map<String, Object> params1 = new HashMap<>();
        if (DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.IOS)) {
            params1.put("start", "50%,45%");
            params1.put("end", "50%,15%");
        } else if (DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.ANDROID)) {
            params1.put("start", "50%,15%");
            params1.put("end", "50%,45%");
        }
        params1.put("operation", "zoom");
        Object result1 = DeviceUtils.getQAFDriver().executeScript(Constants.MOBILETOUCHGESTURE, params1);
    }

    @Then("^I click on \"(.*?)\" and ignore errors$")
    public static void iClick(String locator) {
        try {
            CommonStep.click(locator);
        } catch (Exception ex) {;}
    }

    @Then("^I validate fingerprint on \"(.*?)\"$")
    public void ITestFingerprint(String appName) throws Exception {
        DeviceUtils.switchToContext(Constants.NATIVEAPP);
        DeviceUtils.setSensorAuthentication("name", appName, Constants.SUCCESS, Constants.AUTHFAILED);
    }

    @Then("^I validate Face ID on \"(.*?)\"$")
    public void ITestFaceID(String appName) throws Exception {
        DeviceUtils.switchToContext(Constants.NATIVEAPP);
        DeviceUtils.setSensorAuthentication("name", appName, Constants.SUCCESS, Constants.AUTHFAILED);
    }

    @Then("^I audit accessibility on page \"(.*?)\"$")
    public static void IAuditAccessibility(String pageName)
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        Map<String, Object> params = new HashMap<>();
        params.put("tag", pageName);
        driver.executeScript(Constants.MOBILEACCESSIBILITYAUDIT, params);
    }

    @When("^I start device logging$")
    public void iStartDeviceLogging()
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        Map<String, Object> params = new HashMap<>();
        driver.executeScript(Constants.MOBILELOGSSTART, params);
    }

    @Then("^I stop device logging$")
    public void iStopDeviceLogging()
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        Map<String, Object> params = new HashMap<>();
        driver.executeScript(Constants.MOBILELOGSSTOP, params);
    }

    @When("^I start device vital monitoring every \"(.*?)\" seconds$")
    public void iStartDeviceVitals(String interval)
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        Map<String, Object> params = new HashMap<>();
        params.put("sources", "Device");
        params.put("interval", interval);
        params.put("monitors", "all");
        driver.executeScript(Constants.MOBILEMONITORSTART, params);
    }

    @Then("^I stop device vitals$")
    public void iStopDeviceVitals()
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        Map<String, Object> params = new HashMap<>();
        driver.executeScript(Constants.MOBILEMONITORSTOP, params);
    }

    @When("^I set the device packet loss to \"(.*?)\" Percent$")
    public void iSetTheDevicePacketLossPercentage(String packetLoss)
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        Map<String, Object> params = new HashMap<>();
        params.put(Constants.PACKETLOSS, packetLoss);
        driver.executeScript(Constants.MOBILEVNETWORKSTART, params);
        params.remove(Constants.PACKETLOSS);
    }

    @When("^I update the virtual network to \"(.*?)\"$")
    public void iUpdateTheVirtualNetworkTo(String profile)
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        Map<String, Object> params = new HashMap<>();
        params.put(Constants.PROFILE, profile);
        driver.executeScript(Constants.MOBILEVNETWORKUPDATE, params);
        params.remove(Constants.PROFILE);
    }

    @When("^I receive a phone call$")
    public void iReceiveAPhoneCall()
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        Map<String, Object> params = new HashMap<>();
        params.put(Constants.PROPERTY, Constants.DEVICEID);
        String deviceId = (String) driver.executeScript(Constants.MOBILEHANDSETINFO, params);
        params.remove(Constants.PROPERTY);
        params.put(Constants.TOHANDSET, deviceId);
        driver.executeScript(Constants.MOBILEGATEWAYCALL, params);
        params.remove(Constants.TOHANDSET);
    }

    @When("^I receive an SMS with the text: \"(.*?)\"$")
    public void iReceiveAnSMSWithTheText(String message)
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        Map<String, Object> params = new HashMap<>();
        params.put(Constants.PROPERTY, Constants.DEVICEID);
        String deviceId = (String) driver.executeScript(Constants.MOBILEHANDSETINFO, params);
        params.remove(Constants.PROPERTY);
        params.put(Constants.TOHANDSET, deviceId);
        params.put(Constants.BODY, message);
        driver.executeScript(Constants.MOBILEGATEWAYSMS, params);
        params.remove(Constants.TOHANDSET);
        params.remove(Constants.BODY);
    }

    @Then("^I take a device screenshot$")
    public void iTakeADeviceScreenshot()
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        driver.getScreenshotAs(OutputType.BYTES);
    }

    @Then("^I visually press on \"(.*?)\"$")
    public void IVisualPressOn(String inField) throws Exception {
        Map<String, Object> params = new HashMap<>();
        params.put("label",inField);
        DeviceUtils.getQAFDriver().executeScript(Constants.MOBILEBUTTONTEXTCLICK, params);
    }

    @Then("^I visually enter \"(.*?)\" on \"(.*?)\"$")
    public void IVisualEnter(String inText, String inField) throws Exception {
        Map<String, Object> params = new HashMap<>();
        params.put("label",inField);
        params.put("text",inText);
        DeviceUtils.getQAFDriver().executeScript(Constants.MOBILEEDITTEXTSET, params);
    }

    @Then("^I visually press on button \"(.*?)\"$")
    public void IVisualPressOnButton(String inField) throws Exception {
        Map<String, Object> params = new HashMap<>();
        params.put("label", inField);
        DeviceUtils.getQAFDriver().executeScript(Constants.MOBILEBUTTONIMAGECLICK, params);
    }

}
