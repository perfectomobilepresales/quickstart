@Calculator
Feature: Calculator

  @CalculatorAddition
  Scenario: Adding 3 to 5
    Given I start application by name "Calculator"
    When clear Calculator
    When add "3" to "5"
    Then result should be "8"
    And I close application by name "Calculator"

  @CalculatorAddition
  Scenario: Adding 6 to 7
    Given I start application by name "Calculator"
    When clear Calculator
    When add "6" to "7"
    Then result should be "13"
    And I close application by name "Calculator"