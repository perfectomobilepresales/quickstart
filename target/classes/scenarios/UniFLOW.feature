@UniFLOW
  Feature: UniFLOW demos

    @UniFLOW_start
    Scenario Outline: UniFLOW online start
      Given I try to close application by id "com.ntware.mobile.uniFLOWOnline" and ignore error
      And I start application by id "com.ntware.mobile.uniFLOWOnline"
      And I switch to webview context
      And I click on "next"
      And I click on "manual_login"
      When I enter "<tenant_url>" to "tenant_url"
      And I click on "next"
      And I wait "120" seconds for "account_name" to appear
      Then I should see text "uniFLOW Online Account"
      And I click on "account_name"
      And I enter "<username>" to "login_id"
      And I click on "signin_next"
      And I enter "<password>" to "login_pass"
      When I click on "sign_in"
      And I click on "yes"
      Then I wait "10" seconds for "dashboard" to appear
      Examples:
        | tenant_url                                  | username                                                     | password         |
        | lc1dn20210119152611618.ea.uniflowonline.com | rootadmin_lc1dn20210119152611618@eaaduniflow.onmicrosoft.com | Eisy339W?SA#bnts |

    @UniFLOW_print_photo
    Scenario: UniFLOW online print photo
      Given I try to close application by id "com.ntware.mobile.uniFLOWOnline" and ignore error
      When I start application by id "com.ntware.mobile.uniFLOWOnline"
      And I switch to webview context
      Then I wait "10" seconds for "print" to appear
      And I click on "print"
      And I start inject "PRIVATE:kyoto.jpg" image to application name "uniFLOW Online Print & Scan"
      And I switch to webview context
      When I click on "take_picture"
      And I switch to native context
      And I click on "while_using_the_app"
      And I click on "allow_access"
      And I click on "image_next1"
      And I click on "image_next2"
      And I switch to webview context
      And I click on "add_to_queue"
      Then I click on "my_queue"
      And I click on "select_all"
      And I click on "recycle_bin"
      And I click on "delete_job"

    @UniFLOW_cleanup
    Scenario: UniFLOW online cleanup
      Given I uninstall application by id "com.ntware.mobile.uniFLOWOnline"