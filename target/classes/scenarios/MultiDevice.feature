@MultiDevice
Feature: Multi Device Demo Scenarios

  @2DevicesDemo
  Scenario: 2 Devices Demo
    Given I switch to driver "perfectoRemote"
    And I switch to driver "perfectodeviiRemote"
    And I switch to driver "perfectoRemote"
    And I open browser to webpage "https://www.perfecto.io"
    And I switch to driver "perfectodeviiRemote"
    And I open browser to webpage "https://www.perforce.com"

  @3DevicesDemo
  Scenario: 3 Devices Demo
    Given I switch to driver "perfectoRemote"
    And I switch to driver "perfectodeviiRemote"
    And I switch to driver "perfectodeviiiRemote"
    And I switch to driver "perfectoRemote"
    And I open browser to webpage "https://www.perfecto.io"
    And I switch to driver "perfectodeviiRemote"
    And I open browser to webpage "https://www.perforce.com"
    And I switch to driver "perfectodeviiiRemote"
    And I open browser to webpage "https://www.google.com"
